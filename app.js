'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

// Cargar archivos de rutas

// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// CORS

// Rutas 

app.get('/', (request, response) => {
    response.status(200).send(
        '<h1>Pagina principal</h1>', 
    );
});

app.get('/test', (request, response) => {
    response.status(200).send({
        message: 'Hola mundo desde el servidor de NodeJS', 
    });
});

// Exportar
module.exports = app;